package cringe;

public class Main {
  public static void main(String... args) {
    Boy me = new Boy();
    Girl you = new Girl();

    me.acquireCrush(you);
    you.acquireCrush(me);

    if (!me.hasGirlfriend() && me.hasCrushOn(you)) {
      me.askOut(you);
    }
  }
}