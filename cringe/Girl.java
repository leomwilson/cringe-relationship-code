package cringe;

public class Girl extends Person {
  private Boy boyfriend;
  private Boy crush;

  public Girl() {
    super();
  }

  public void acquireCrush(Boy boy) {
    this.crush = boy;
  }

  public boolean hasBoyfriend() {
    return boyfriend != null;
  }

  public boolean hasCrushOn(Boy boy) {
    return crush == boy;
  }

  public boolean askedOut(Boy boy) throws IAlreadyHaveABoyfriendException {
    if (!hasBoyfriend()) {
      if (hasCrushOn(boy)) {
        boyfriend = boy;
        return true;
      } else {
        return false;
      }
    } else {
      throw new IAlreadyHaveABoyfriendException();
    }
  }
}
