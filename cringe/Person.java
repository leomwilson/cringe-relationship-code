package cringe;

public abstract class Person {
  private Emotion emotion;

  public Person() {
    emotion = Emotion.NEUTRAL;
  }

  public void cry() {
    emotion = Emotion.SAD;
  }

  public void smile() {
      emotion = Emotion.HAPPY;
  }
}
