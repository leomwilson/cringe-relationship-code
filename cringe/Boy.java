package cringe;

public class Boy extends Person {
  private Girl girlfriend;
  private Girl crush;

  public Boy() {
    super();
  }

  public void acquireCrush(Girl girl) {
    this.crush = girl;
  }

  public boolean hasGirlfriend() {
    return girlfriend != null;
  }

  public boolean hasCrushOn(Girl girl) {
    return crush == girl;
  }

  public void askOut(Girl girl) {
    try {
      if (girl.askedOut(this)) {
        girlfriend = girl;
        smile();
        System.out.println("here");
      } else {
        cry();
      }
    } catch (IAlreadyHaveABoyfriendException f) {
      cry();
    }
  }
}
